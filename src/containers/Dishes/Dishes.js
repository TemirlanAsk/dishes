import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux';
import {Button, ButtonGroup, Col, Grid, Image, Panel, Row} from "react-bootstrap";
import DishModal from "../../components/DishModal/DishModal";
import DishForm from "../../components/DishForm/DishForm";
import {createDish, deleteDish, editDish, fetchDishes} from "../../store/actions/dishes";


class Dishes extends Component {
        state = {
            show : false,
            editedDish: null

        };

        componentDidMount() {
            this.props.onFetchDishes();
        }


        handleClose = () => {
            this.setState({show: false});
        };

        openAddDishModal = () => {
            this.setState({show: true})
        };

         handleEditClose = () => {
            this.setState({editedDish: null});
         };


        createDishHandler = dish => {
            this.props.onCreateDish(dish).then(this.handleClose)
        };

        selectEditedDish = (dish) => {
            this.setState({editedDish: dish});
        };

        editDishHandler =  dish => {
            console.log(dish);
            const id = dish.id;
            delete dish.id;

            this.props.onEditDish(id, dish)
                .then(this.handleEditClose)
        };


        deleteDishHandler = id => {
            this.props.onDeleteDish(id)
        };



render()  {
return(
    <Fragment>
    <Grid>
        <Row className="show-grid">
            <Col>
            <h2>Dishes  <Button bsStyle="primary"
            onClick={this.openAddDishModal}>Add dish</Button></h2>
                {
                    this.props.dishes.map(dish => (
                        <Panel key={dish.id}>
                            <Panel.Body>
                                <Image style={{width: '150px'}}  src={dish.image} thumbnail />
                                <span style={{margin: '10px'}}>{dish.title}</span>
                                <strong>{dish.price} KGS</strong>
                                <ButtonGroup style={{float: 'right'}}>
                                    <Button onClick={() => this.selectEditedDish(dish)} >Edit</Button>
                                    <Button
                                        bsStyle="danger"
                                        onClick={() => this.deleteDishHandler(dish.id)}>Delete
                                    </Button>
                                </ButtonGroup>
                                </Panel.Body>
                        </Panel>
                    ))
                }

            </Col>
        </Row>
    </Grid>
        <DishModal show={this.state.show} onHide={this.handleClose} title="Add new dish">
            <DishForm onFormSubmitted={this.createDishHandler}/>
        </DishModal>

        <DishModal show={!!this.state.editedDish} onHide={this.handleEditClose} title="Edit dish">
            {this.state.editedDish ?
                <DishForm dish={this.state.editedDish} onFormSubmitted={this.editDishHandler}/> : null
            }
        </DishModal>


</Fragment>
)
}
}

const mapStateToProps = state => {
        return {
            dishes: state.dishes
        }
};

const mapDispatchToProps = dispatch => {
        return {
            onCreateDish: dish => dispatch(createDish(dish)),
            onFetchDishes: () => dispatch(fetchDishes()),
            onEditDish: (id, dish) => dispatch(editDish(id, dish)),
            onDeleteDish: id => dispatch(deleteDish(id))
        }
};





export default connect(mapStateToProps, mapDispatchToProps)(Dishes);