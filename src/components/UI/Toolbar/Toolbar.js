import React from 'react';
import './Toolbar.css'
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";



const Toolbar = () => (
        <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                    <LinkContainer to="/" exact><a>Pizzaria Admin</a></LinkContainer>
                </Navbar.Brand>
            </Navbar.Header>
            <Nav pullRight>
                <LinkContainer to="/" exact>
                <NavItem >Dishes</NavItem>
                </LinkContainer>
                <LinkContainer to="/orders" exact>
                <NavItem>Orders</NavItem>
                </LinkContainer>
            </Nav>
        </Navbar>

);

export default Toolbar;