import React, {Component} from "react";
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Thumbnail} from "react-bootstrap";


class  DishForm extends Component {
    state = {
        title: '',
        price: '',
        image: ''
    };

    constructor(props) {
        super(props);

        console.log('here', props);

        if (props.dish) {
          this.state = {...props.dish}
        }
    }

    submitForm = event => {
      event.preventDefault();
      this.props.onFormSubmitted(this.state);
    };

    valueChanged = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };


    render(){
        let imagePreview = null;

        if(this.state.image) {
            imagePreview = (
                <FormGroup controlId="dishImage">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image Preview
                    </Col>
                    <Col sm={10}>
                        < Thumbnail style={{minHeight: 100}} src={this.state.image} />
                    </Col>
                </FormGroup>
            )
        }


        return(
            <Form horizontal onSubmit={this.submitForm}>
                <FormGroup controlId="dishTitle">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="title" required
                            placeholder="Enter title"
                            name="title"
                            value={this.state.title}
                            onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup controlId="dishPrice">
                    <Col componentClass={ControlLabel} sm={2}>
                        Price
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="number" min="0" required
                            placeholder="Enter dish price"
                            name="price"
                            value={this.state.price}
                            onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup controlId="dishImage">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="url" required
                            placeholder="Enter dish image url"
                            name="image"
                            value={this.state.image}
                            onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                {imagePreview}

                <FormGroup>
                    <Col smOffset={2} sm={10}>

                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}

export default DishForm;